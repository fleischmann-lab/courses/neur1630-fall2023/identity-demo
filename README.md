# Demo for Identity module on JupyterHub resources

[![Launch JupyterHub](https://img.shields.io/badge/identity-demo-orange?style=flat-square)](https://neur1630.jupyter.brown.edu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.com%2Ffleischmann-lab%2Fcourses%2Fneur1630-fall2023%2Fidentity-demo&urlpath=lab%2Ftree%2Fidentity-demo%2F&branch=main)

Click on the icon above (or [here](https://neur1630.jupyter.brown.edu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.com%2Ffleischmann-lab%2Fcourses%2Fneur1630-fall2023%2Fidentity-demo&urlpath=lab%2Ftree%2Fidentity-demo%2F&branch=main)) to launch Jupyter Hub.

You can also go to `notebooks` folder.

## A. BrainCellData

The single-cell data are from [`braincelldata`](https://braincelldata.org/) (already downloaded to `/home/joyvan/shared/braincelldata` directory).

The notebook `notebooks/A1-braincelldata-subset.ipynb` first subsets the data, and will save into `data/braincelldata` under your user directory (**not** under the shared one)

The notebook `notebooks/A2-braincelldata-analysis.ipynb` will load the subset data in, and go through the different steps from preprocessing to clustering and identifying differential expressed genes.

These notebooks are created by Tuan Pham, with consulting with codes from the Fleischmann lab (and collaborators), as well as other sources (see more in notebook).

## B. Interneurons

The dataset is from Dr. Fleischmann's lab, experiment and analysis led by Dr. Sara Zeppill. The dataset is made available at `/home/shared/data/zeppilli-etal-2023` directory.

The notebooks `notebooks/B1-interneurons-reprocess.ipynb` and `notebooks/B2-Interneurons-cluster.ipynb` re-processes this dataset by sub-setting the interneurons and re-clustering them.

These notebooks are created by Emily DeNunzio, with consulting with codes from the Fleischmann lab (and collaborators).

