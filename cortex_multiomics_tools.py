# Operating system calls and path manipulations
# Fast searching in ordered sequences
import bisect
import datetime
import os

# Pretty printing
import pprint
import sys

import anndata2ri

# Figure making
import matplotlib.pyplot as plt

# Maths
import numpy as np

# We always need DataFrames
import pandas as pd

# Get TSS annotations
import pybiomart as pbm

# Genome coordinates made easy
import pyranges as pr

# SCRAN normalization
# os.environ['R_HOME'] = "/opt/anaconda3/envs/scanpy/lib/R"
import rpy2.rinterface_lib.callbacks
import rpy2.robjects as ro
import scipy.sparse

# Single cell analysis
import scanpy as sc

# Preventing bytecode makes it easier to update values and reload the file
sys.dont_write_bytecode = True


# CONSTANTS

# Input and output
SHARED_DATA_PATH = "/home/jovyan/shared/data/zeppilli-etal-2023/" # for shared data path to access source data
DATA_PATH = "data/interneurons" # to save & access subsequent processed data

# Figures
FIGURES_PATH = "figures/interneurons" # figure path

# The official Sara-certified colors for our UMAPs, violin plots, etc.
CORTICAL_AREA_COLORS = {
    "Anterior_Paleocortex": "#1b9e77",
    "Posterior_Paleocortex": "#d95f02",
    "Peri_Paleocortex": "#7570b3",
    "Neocortex": "#e7298a",
}
# Sometimes we group anterior and posterior paleocortex
# 'Paleocortex': '#6c584cCC',


COARSE_SUPERTYPE_COLORS = {
    "Astro": "#997b66",
    "Pyr": "#2bd9feA6",
    "Micro": "#90be6d",
    "Immature-like": "#9e2a2bB3",
    "IN1_MGE": "#b5179e",  # a6
    "IN2_MGE": "#e5b3fe",
    "IN1_CGE": "#6a00f4",  # A6
    "IN2_CGE": "#b298dc",
    "SL": "#e76f51B3",
    "VLMC": "#656d4a",  # B6
    "Exc_Pappa2+": "#da1e37",
    "Oligo": "#2d6a4f",  # A6
    "OPC": "#06d6a0",
    "OPC_diff": "#bfd8bd",
}


# scRNA-seq filename
RNA_10X_FILENAME = "filtered_feature_bc_matrix.h5"

# Info on where to find all the CellRanger output
METADATA = [
    {
        "cortical_area": "Anterior_Paleocortex",
        "mouse_strain": "C57BL-6",
        "mouse_strain_label": "lab",
        "experiment": "multiome",
        "path_10x_out": "anterior_pcx",
        "replicates": [
            {"name": "A0", "folder_name": "Atest", "mouse_id": 0, "gender": "male"},
            {"name": "A1", "folder_name": "A1", "mouse_id": 1, "gender": "female"},
            {"name": "A2", "folder_name": "A2", "mouse_id": 2, "gender": "female"},
            {"name": "A3", "folder_name": "A3", "mouse_id": 3, "gender": "male"},
            {"name": "A4", "folder_name": "A4", "mouse_id": 4, "gender": "male"},
            {"name": "A6", "folder_name": "A6", "mouse_id": 6, "gender": "female"},
            {"name": "A9", "folder_name": "A9", "mouse_id": 9, "gender": "female"},
            {"name": "A10", "folder_name": "A10", "mouse_id": 10, "gender": "male"},
            {"name": "A11", "folder_name": "A11", "mouse_id": 11, "gender": "male"},
        ]
        # A8 is bad replicate, not included
    },
    {
        "cortical_area": "Posterior_Paleocortex",
        "mouse_strain": "C57BL-6",
        "mouse_strain_label": "lab",
        "experiment": "multiome",
        "path_10x_out": "posterior_pcx",
        "replicates": [
            {"name": "P1", "folder_name": "P1", "mouse_id": 1, "gender": "female"},
            {"name": "P2", "folder_name": "P2", "mouse_id": 2, "gender": "female"},
            {"name": "P4", "folder_name": "P4", "mouse_id": 4, "gender": "male"},
            {"name": "P5", "folder_name": "P5", "mouse_id": 5, "gender": "male"},
            {"name": "P6", "folder_name": "P6", "mouse_id": 6, "gender": "female"},
            {"name": "P7", "folder_name": "P7", "mouse_id": 7, "gender": "male"},
            {"name": "P8", "folder_name": "P8", "mouse_id": 8, "gender": "female"},
            {"name": "P9", "folder_name": "P9", "mouse_id": 9, "gender": "female"},
            {"name": "P10", "folder_name": "P10", "mouse_id": 10, "gender": "male"},
            {"name": "P11", "folder_name": "P11", "mouse_id": 11, "gender": "male"},
        ],
    },
    {
        "cortical_area": "Peri_Paleocortex",
        "mouse_strain": "C57BL-6",
        "mouse_strain_label": "lab",
        "experiment": "multiome",
        "path_10x_out": "transition_area",
        "replicates": [
            {"name": "T1", "folder_name": "T1", "mouse_id": 1, "gender": "female"},
            {"name": "T4", "folder_name": "T4", "mouse_id": 4, "gender": "male"},
            {"name": "T5", "folder_name": "T5", "mouse_id": 5, "gender": "male"},
            {"name": "T6", "folder_name": "T6", "mouse_id": 6, "gender": "female"},
            {"name": "T7", "folder_name": "T7", "mouse_id": 7, "gender": "male"},
        ],
    },
    {
        "cortical_area": "Neocortex",
        "mouse_strain": "C57BL-6",
        "mouse_strain_label": "lab",
        "experiment": "multiome",
        "path_10x_out": "neocortex",
        "replicates": [
            {"name": "N1", "folder_name": "N1", "mouse_id": 1, "gender": "female"},
            {"name": "N4", "folder_name": "N4", "mouse_id": 4, "gender": "male"},
            {"name": "N6", "folder_name": "N6", "mouse_id": 6, "gender": "female"},
            {"name": "N7", "folder_name": "N7", "mouse_id": 7, "gender": "male"},
        ]
        # N4 looks bad, use with awareness
    },
    #     # Adult wild mice
    {
        "cortical_area": "Anterior_Paleocortex",
        "mouse_strain": "wild_derived",
        "mouse_strain_label": "wild",
        "experiment": "multiome",
        "path_10x_out": "anterior_pcx_wild",
        "replicates": [
            {"name": "A1W", "folder_name": "A1W", "mouse_id": 1, "gender": "male"},
            {"name": "A2W", "folder_name": "A2W", "mouse_id": 2, "gender": "female"},
        ],
    },
    {
        "cortical_area": "Anterior_Paleocortex",
        "mouse_strain": "wild_derived",
        "mouse_strain_label": "wild",
        "experiment": "rna-only",
        "path_10x_out": "anterior_pcx_wild",
        "names": ["A3W", "A4W", "A5W", "A6W"],
        "replicates": [
            {"name": "A3W", "folder_name": "A3W", "mouse_id": 3, "gender": "male"},
            {"name": "A4W", "folder_name": "A4W", "mouse_id": 4, "gender": "male"},
            {"name": "A5W", "folder_name": "A5W", "mouse_id": 5, "gender": "female"},
            {"name": "A6W", "folder_name": "A6W", "mouse_id": 6, "gender": "female"},
        ],
    },
    {
        "cortical_area": "Posterior_Paleocortex",
        "mouse_strain": "wild_derived",
        "mouse_strain_label": "wild",
        "experiment": "multiome",
        "path_10x_out": "posterior_pcx_wild",
        "replicates": [
            {"name": "P1W", "folder_name": "P1W", "mouse_id": 1, "gender": "male"},
            # P2W and P3W are bad samples, only 160 cells remain for each replicate
            # after QC. We do not use them for the moment
        ],
    },
    {
        "cortical_area": "Posterior_Paleocortex",
        "mouse_strain": "wild_derived",
        "mouse_strain_label": "wild",
        "experiment": "rna-only",
        "path_10x_out": "posterior_pcx_wild",
        "replicates": [
            {"name": "P4W", "folder_name": "P4W", "mouse_id": 4, "gender": "male"},
            {"name": "P5W", "folder_name": "P5W", "mouse_id": 5, "gender": "female"},
            {"name": "P6W", "folder_name": "P6W", "mouse_id": 6, "gender": "female"},
        ],
    },
    {
        "cortical_area": "Neocortex",
        "mouse_strain": "wild_derived",
        "mouse_strain_label": "wild",
        "experiment": "multiome",
        "path_10x_out": "ncx_wild",
        "replicates": [
            {"name": "N2W", "folder_name": "N2W", "mouse_id": 2, "gender": "female"},
        ],
    },
    {
        "cortical_area": "Neocortex",
        "mouse_strain": "wild_derived",
        "mouse_strain_label": "wild",
        "experiment": "rna-only",
        "path_10x_out": "ncx_wild",
        "replicates": [
            {"name": "N6W", "folder_name": "N6W", "mouse_id": 6, "gender": "female"},
        ],
    },

]


# Below are a whole bunch of paths predefined to make sure the output files of
# ATAC analyses are always in specific folders

# Default file name for 10x atac results
FRAGMENTS_FILENAME = "atac_fragments.tsv.gz"

# Parts of the genome are forbidden
BLOCKLIST_FILENAME = "../../repositories/" "pycisTopic/blacklist/mm10-blacklist.v2.bed"

# Bed and bigwig pseudobulk files, one file per scRNA-seq defined supertypes
LOCAL_BED_PATH = "pseudobulk_bed"
LOCAL_BIGWIG_PATH = "pseudobulk_bigwig"

# Peaks are called from pseudobulk by 'macs2'
MACS_PATH = "pseudobulk_macs_peaks"
# and integrated using an iterative approach into consensus peaks
# (~consensus regions)
PEAK_PATH = os.path.join(DATA_PATH, "scatac_consensus_regions")

# Per replicate, record which barcodes pass QC filters
QUALITY_CONTROL_PATH = "quality_control"

# LDA topics computed by 'mallet'. The final slash-forward is important!
MALLET_TMP = "mallet_tmp/"
# Mallet results are stored per dataset
__MALLET_BASEPATH = os.path.join(DATA_PATH, "scatac_mallet_topics")
MALLET_PATH = {
    "lab": os.path.join(__MALLET_BASEPATH, "lab"),
    "lab_sl-pyr-in-cr": os.path.join(__MALLET_BASEPATH, "lab_sl-pyr-in-cr"),
    "lab-wild": os.path.join(__MALLET_BASEPATH, "lab-wild"),
    "neonatal": os.path.join(__MALLET_BASEPATH, "neonatal"),
}

# cisTopic objects that one can analyze
CISTOPIC_PATH = os.path.join(DATA_PATH, "scatac_cistopic")
# Base objects that lack the dimension reduction of (mallet) topics
BASE_CISTOPIC_PATH = os.path.join(DATA_PATH, "scatac_cistopic_merged_base")
# Single models, to be merged into a base object
SINGLE_CISTOPIC_PATH = os.path.join(DATA_PATH, "scatac_cistopic_single_models")

# Imputed accessibility computed from topics
IMPUTED_ACCESS_PATH = os.path.join(DATA_PATH, "scatac_imputed_accessibility")
# Gene activity computed from imputed access
GENE_ACTIVITY_PATH = os.path.join(DATA_PATH, "scatac_gene_activity")
# Differentially accessible regions
DIFF_ACCESS_PATH = os.path.join(DATA_PATH, "scatac_diff_access_regions")

# cistarget databases
CISTARGET_DB_PATH = os.path.join(DATA_PATH, "cistarget_dbs")


# Distributed computing framework 'ray' needs a temporary folder to store
# session info
RAY_TMP = "/home/acrombac/ray_spill"


# FUNCTIONS TO MAKE LIFE EASIER

# Normalisation adapted from Fabian Theis' `scib` repo
# See: https://github.com/theislab/scib/blob/main/scib/preprocessing.py#L149
def scran_size_factors(adata, min_mean=0.1, log=True, precluster=True, sparsify=True):
    """
    Connect to R to compute library size factors using SCRAN (v3.12).
    """
    # Check for 0 count cells
    if np.any(adata.X.sum(axis=1) == 0):
        raise ValueError(
            "found 0 count cells in the AnnData object."
            " Please filter these from your dataset."
        )

    # Check for 0 count genes
    if np.any(adata.X.sum(axis=0) == 0):
        raise ValueError(
            "found 0 count genes in the AnnData object."
            " Please filter these from your dataset."
        )

    if sparsify:
        # Massive speedup when working with sparse matrix
        # AC: ??? quick fix; HVG doesn't work on dense matrix
        if not scipy.sparse.issparse(adata.X):
            adata.X = scipy.sparse.csr_matrix(adata.X)

    # Activate conversion AnnData-SCE and load `scran` library
    print("Loading SCRAN library")
    anndata2ri.activate()
    # ro.r('library("scran")')
    ro.r('suppressMessages(library("scran"))')

    # Convert to CSC if possible. See https://github.com/MarioniLab/scran/issues/70
    X = adata.X.T
    is_sparse = False
    if scipy.sparse.issparse(X):
        is_sparse = True
        X = X.tocoo() if X.nnz > 2**31 - 1 else X.tocsc()
    # AC: Assign matrix of counts to global R variable "data_mat"
    ro.globalenv["data_mat"] = X

    # Preliminary clustering for differentiated normalisation (or not)
    if precluster:
        old_verbosity = sc.settings.verbosity
        sc.settings.verbosity = 1
        print("Preclustering...")
        # We cluster on a copy of the data and then delete it
        adata_pp = adata.copy()
        sc.pp.normalize_per_cell(adata_pp, counts_per_cell_after=1e4)
        sc.pp.log1p(adata_pp)
        # PCA over 15 components, should be okay for prelim clustering
        sc.pp.pca(adata_pp, n_comps=15, svd_solver="arpack")
        sc.pp.neighbors(adata_pp)
        # Resolution of 0.5 is a reasonable default value
        sc.tl.leiden(adata_pp, key_added="groups", resolution=0.5)
        # Set global R variable "input_groups" to the prelim clustering results
        ro.globalenv["input_groups"] = adata_pp.obs["groups"]
        size_factors = ro.r(
            "sizeFactors(computeSumFactors(SingleCellExperiment("
            "list(counts=data_mat)), clusters=input_groups,"
            f" min.mean={min_mean}))"
        )
        del adata_pp
        sc.settings.verbosity = old_verbosity

    else:
        # Vanilla "compute size factors"
        print("No preclustering...")
        size_factors = ro.r(
            "sizeFactors(computeSumFactors(SingleCellExperiment("
            f"list(counts=data_mat)), min.mean={min_mean}))"
        )

    # Free memory in R
    ro.r("rm(list=ls())")
    ro.r(
        "suppressMessages(lapply(names(sessionInfo()$loadedOnly), require, character.only = TRUE))"
    )
    ro.r(
        'invisible(lapply(paste0("package:", names(sessionInfo()$otherPkgs)), '
        "detach, character.only=TRUE, unload=TRUE))"
    )
    ro.r("gc()")
    # And deactivate automatic AnnData-SCE conversion
    anndata2ri.deactivate()
    return pd.Series(size_factors, index=adata.obs.index)


def scran_normalize(
    adata, min_mean=0.1, log=True, precluster=True, batch="replicate", sparsify=True
):
    # For each replicate, calculate size factors
    size_factors = []
    for name in adata.obs[batch].unique():
        print(f"Normalizing {name}", end=": ")
        adata_batch = adata[adata.obs[batch] == name].copy()
        # Filter out any genes that are all zeros in this batch
        sc.pp.filter_genes(adata_batch, min_cells=1)
        # Let SCRAN compute size factors
        size_factors.append(
            scran_size_factors(adata_batch, min_mean, log, precluster, sparsify)
        )
        del adata_batch

    # Modify adata given the just computed size_factors
    adata.obs["size_factors"] = pd.concat(size_factors)
    adata.X /= adata.obs["size_factors"].values[:, None]
    if log:
        print("Log1p-transformation after normalization.")
        sc.pp.log1p(adata)
    else:
        print("No log1p-transformation performed after normalization.")

    if not scipy.sparse.issparse(adata.X):
        # (Re)convert to sparse, because maths operation always converts to dense
        adata.X = scipy.sparse.csr_matrix(adata.X)
    # Store the full data set in 'raw' as log-normalised data for statistical testing
    adata.raw = adata
    return adata


def mark_highly_variable_genes_per_replicate(
    adata,
    ignore_list,
    min_mean=0.0,
    max_mean=100.0,
    min_disp=0.05,
    n_bins=5,
    batch="replicate",
    dry_run=True,
):
    """
    min_mean: minimal level of gene expression to consider.
    max_mean: maximal level of gene expression.
    min_disp: minimal level of dispersion to consider.
    n_bins: number of bins to divide the distribution of expression levels over.
            The bins go from low mean expression levels to high ones.

    The default parameter values were established through trial and error. They
    worked for my particular data set, and they will have to be recalibrated for
    each data set.
    """
    old_verbo = sc.settings.verbosity
    # I want lots of feedback from the algorithm
    if dry_run:
        sc.settings.verbosity = 4
        filter_result = sc.pp.highly_variable_genes(
            adata,
            min_mean=min_mean,
            max_mean=max_mean,
            min_disp=min_disp,
            max_disp=20.0,
            n_bins=n_bins,
            batch_key=batch,
            inplace=False,
        )

        # Plot the filtering result, even if I do not find the plots very intuitive.
        sc.pl.highly_variable_genes(filter_result)
        sc.settings.verbosity = old_verbo
        return filter_result

    else:
        # Do the actual filtering
        sc.pp.highly_variable_genes(
            adata,
            min_mean=min_mean,
            max_mean=max_mean,
            min_disp=min_disp,
            max_disp=20.0,
            n_bins=n_bins,
            batch_key=batch,
        )
        # If we have an ignore list, set HVG properties to zero and False
        adata.var.loc[
            ignore_list,
            [
                "highly_variable_nbatches",
                "highly_variable_intersection",
                "highly_variable",
            ],
        ] = (0, False, False)

    return adata


def select_top_n_highly_variable_genes(data, top=2000):
    """
    Given genes per batch (replicate) marked for being highly variable, find
    the ones that are present across as many batches as possible.
    """
    aux = data.var[data.var["highly_variable"]][
        "highly_variable_nbatches"
    ].value_counts()
    hvg_nbatch = sorted(aux.to_dict().items(), key=lambda x: x[0], reverse=True)

    # Linear search for how many nbatches we need
    sum_ngenes = np.cumsum([x[1] for x in hvg_nbatch])
    if top > sum_ngenes[-1]:
        print("Cannot select more genes than marked as highly variable.")
        return data
    i_nbatch = bisect.bisect(sum_ngenes, top)

    # Set all genes to False
    data.var["highly_variable"] = False

    # Take the full nbatches
    if i_nbatch > 0:
        data.var["highly_variable"] = (
            data.var["highly_variable_nbatches"] > hvg_nbatch[i_nbatch][0]
        )
        top -= sum_ngenes[i_nbatch - 1]

    # Take a partial nbatch, sorted by normalized dispersion
    aux = data.var[data.var["highly_variable_nbatches"] == hvg_nbatch[i_nbatch][0]][
        "dispersions_norm"
    ]
    # Note: we could use np.argpartition here to speed things up if needed
    data.var.loc[aux.sort_values()[-top:].index, "highly_variable"] = True
    return data


def scale_per_replicate(data, batch="replicate"):
    """
    Scale per replicate to z-score, which helps comparing between cells (but
    hinders comparing between genes).
    """

    def __split_replicates(data, batch):
        # Heavily inspired on scib package of Theis lab
        split = []
        batch_categories = data.obs[batch].unique()
        for b in batch_categories:
            split.append(data[data.obs[batch] == b].copy())
        return split

    def __merge_replicates(data_list, batch):
        # The new ad.concat does not take into account the many var and obs fields
        data = data_list[0].concatenate(
            *data_list[1:], index_unique=None, batch_key="aux", uns_merge="same"
        )
        # 'aux' was only a temporary column, we already have 'replicate' as a batch key
        del data.obs["aux"]

        # Removing 'mean-#' and 'std-#' columns... Or are they going to be useful?
        for i in range(len(data.obs[batch].unique())):
            del data.var["mean-{}".format(i)]
            del data.var["std-{}".format(i)]
        return data

    # Split and merge
    split_data = __split_replicates(data, batch)
    for s in split_data:
        sc.pp.scale(s)
    scaled_data = __merge_replicates(split_data, batch)

    # Check if any 'uns' fields were forgotten (like color schemes)
    scaled_data.uns.update(data.uns)
    return scaled_data


def __which_embedding(embedding):
    """Auxilary function for cluster comparison."""
    if embedding == "pca":
        X_name = "X_pca"
    elif embedding == "harmony":
        X_name = "X_pca_harmony"
    else:
        raise ValueError("Unknown embedding. Choose 'pca' or 'harmony'")
    # Default names in AnnData given by scanpy algorithms
    return (X_name, "neighbors", "distances", "connectivities")


def compare_clusterings_compute(
    data,
    embedding,
    seed,
    n_neighbors=[10, 20, 50, 100],
    resolutions=[0.2, 0.5, 0.9],
    n_pcs=None,
):
    """
    Compute and store everything needed to compare different clustering parameters

    Arguments:
        data: AnnData object
        embedding: 'pca' or 'harmony'
        seed: integer seed to use for UMAP
        n_neighbors (optional): list of values to try as the number of neighbors (default: [10, 20, 50, 100])
        resolutions (optional): list of values to try as the resolution (default: [0.2, 0.5, 0.9])

    Returns: data_cache (dictionary storing all computed values for all parameters)
    """
    # Reduce scanpy's output setting, otherwise will get TONS of output from this function
    old_verbosity = sc.settings.verbosity
    sc.settings.verbosity = 1

    X_name, nb_name, distances_name, connectivities_name = __which_embedding(embedding)
    # Make a grid of neighbors and leiden clustering
    # We store the number of neighbors and resolutions, so plotting doesn't need them as a separate argument
    data_cache = {
        "embedding": embedding,
        "n_neighbors": n_neighbors,
        "resolutions": resolutions,
        "uns_neighbors": {},
        "obsp_distances": {},
        "obsp_connectivities": {},
        "leiden": {},
        "uns_leiden": {},
        "obsm_X_umap": {},
    }

    # Computing and storing
    for nb in n_neighbors:
        if n_pcs is not None:
            sc.pp.neighbors(data, n_pcs=n_pcs, n_neighbors=nb, use_rep=X_name)
        else:
            sc.pp.neighbors(
                data,
                n_pcs=data.uns[nb_name]["params"]["n_pcs"],
                n_neighbors=nb,
                use_rep=X_name,
            )

        # Store the results of the neighborhood computation
        data_cache["uns_neighbors"][nb] = data.uns[nb_name].copy()
        data_cache["obsp_distances"][nb] = data.obsp[distances_name].copy()
        data_cache["obsp_connectivities"][nb] = data.obsp[connectivities_name].copy()

        # Compute UMAP projection, store it
        sc.tl.umap(data, random_state=seed, min_dist=1.0, spread=1.1)
        data_cache["obsm_X_umap"][nb] = data.obsm["X_umap"].copy()

        # Do different clusterings on a given UMAP for a given nr of neighbors
        for res in resolutions:
            sc.tl.leiden(data, resolution=res)
            data_cache["leiden"][(nb, res)] = data.obs["leiden"].copy()
            data_cache["uns_leiden"][(nb, res)] = data.uns["leiden"].copy()

    sc.settings.verbosity = old_verbosity
    return data_cache


def compare_clusterings_plot(data, data_cache, markers, save=None):
    """
    Plot comparisons for clustering parameters

    Arguments:
        data: AnnData object
        data_cache (output of compare_clusterings_compute)
        marker (string: marker gene to plot on umap)
        save (filename to save figure as. If None (default), figure will not be saved)

    In the resulting plot, each row is a set of parameters.
    The first column shows the clusters on a UMAP, with the parameters and number of clusters listed above.
    The second column plots the expression of the chosen marker on the UMAP, with the marker listed above.
    """
    embedding = data_cache["embedding"]
    X_name, nb_name, distances_name, connectivities_name = __which_embedding(embedding)

    n_neighbors = data_cache["n_neighbors"]
    resolutions = data_cache["resolutions"]

    dotsize = 25
    # One row for each set of parameters, three columns:
    # 1. UMAP with clusters labeled,
    # 2-3. UMAP of marker gene expression
    nr = len(n_neighbors) * len(resolutions)
    nc = 1 + len(markers)
    fig, axs = plt.subplots(nrows=nr, ncols=nc, figsize=(5 * nc, 4 * nr))

    for i, nb in enumerate(n_neighbors):
        # Copy from cache to data
        data.uns[nb_name] = data_cache["uns_neighbors"][nb].copy()
        data.obsp[distances_name] = data_cache["obsp_distances"][nb].copy()
        data.obsp[connectivities_name] = data_cache["obsp_connectivities"][nb].copy()
        data.obsm["X_umap"] = data_cache["obsm_X_umap"][nb].copy()

        for j, res in enumerate(resolutions):
            row_idx = i * len(resolutions) + j
            # Copy from cache to data
            data.obs["leiden"] = data_cache["leiden"][(nb, res)].copy()
            data.uns["leiden"] = data_cache["uns_leiden"][(nb, res)].copy()
            nclusters = len(data.obs["leiden"].unique())

            # Plot clusters on umap
            sc.pl.umap(
                data,
                color="leiden",
                size=dotsize,
                title=f"{nb} neighbors, {res} res: {nclusters} clusters",
                legend_loc="on data",
                legend_fontoutline=2,
                show=False,
                ax=axs[row_idx, 0],
            )

            # Plot marker gene on umap
            for k, m in enumerate(markers):
                sc.pl.umap(
                    data,
                    color=m,
                    size=dotsize,
                    title=m,
                    legend_loc="right",
                    # legend_fontoutline=2,
                    show=False,
                    ax=axs[row_idx, 1 + k],
                )

    if save is not None:
        plt.savefig(save)
    return fig, axs


def load_clustering(data, data_cache, nb, res):
    """
    Load selected clustering stored in data_cache

    Arguments:
        data: AnnData object
        data_cache: output of compare_clusterings_compute
        nb: chosen number of neighbors
        res: chosen resolution
    """
    embedding = data_cache["embedding"]
    X_name, nb_name, distances_name, connectivities_name = __which_embedding(embedding)

    # Copy from cache to data
    data.uns[nb_name] = data_cache["uns_neighbors"][nb].copy()
    data.obsp[distances_name] = data_cache["obsp_distances"][nb].copy()
    data.obsp[connectivities_name] = data_cache["obsp_connectivities"][nb].copy()
    data.obsm["X_umap"] = data_cache["obsm_X_umap"][nb].copy()
    data.obs["leiden"] = data_cache["leiden"][(nb, res)].copy()
    data.uns["leiden"] = data_cache["uns_leiden"][(nb, res)].copy()


def get_transcription_start_site_annotations(
    species="mm", tss_as_start=True, strand_as_sign=False
):
    dataset = None
    if species == "mm":
        # For mouse (mm39)
        # dataset = pbm.Dataset(name='mmusculus_gene_ensembl',
        #                       host='http://www.ensembl.org')
        # For mouse (mm10)
        dataset = pbm.Dataset(
            name="mmusculus_gene_ensembl", host="http://nov2020.archive.ensembl.org/"
        )
    elif species == "hg":
        # For human (hg38)
        dataset = pbm.Dataset(
            name="hsapiens_gene_ensembl", host="http://www.ensembl.org"
        )
        # For human (hg19)
        # dataset = pbm.Dataset(name='hsapiens_gene_ensembl',
        #                       host='http://grch37.ensembl.org/')
    elif species == "dm":
        # For fly (dm6)
        dataset = pbm.Dataset(
            name="dmelanogaster_gene_ensembl", host="http://www.ensembl.org"
        )
    else:
        raise ValueError("Unknown species")

    annot = dataset.query(
        attributes=[
            "chromosome_name",
            "start_position",
            "end_position",
            "strand",
            "transcription_start_site",
            "external_gene_name",
            "transcript_biotype",
        ]
    )
    # Mouse specific?
    my_filter = annot["Chromosome/scaffold name"].str.contains("CHR|GL|JH|MT", na=False)
    annot = annot[~my_filter]

    annot["Chromosome/scaffold name"] = annot["Chromosome/scaffold name"].str.replace(
        r"(\b\S)", r"chr\1"
    )
    # annot['Chromosome/scaffold name'] = \
    #     'chr' + annot['Chromosome/scaffold name'].astype(str)

    if tss_as_start:
        # Rename columns, take TSSs as Start
        annot = annot[
            [
                "chromosome_name",
                "transcription_start_site",
                "end_position",
                "strand",
                "external_gene_name",
                "transcript_biotype",
            ]
        ]
        annot.columns = [
            "Chromosome",
            "Start",
            "End",
            "Strand",
            "Gene",
            "Transcript_type",
        ]
    else:
        # Only rename columns
        annot.columns = [
            "Chromosome",
            "Start",
            "End",
            "Strand",
            "Transcription_Start_Site",
            "Gene",
            "Transcript_type",
        ]
    # Keep only protein-coding genes, remove pseudogenes, lncrna's etc.
    annot = annot[annot.Transcript_type == "protein_coding"]

    if strand_as_sign:
        annot.Strand[annot.Strand == 1] = "+"
        annot.Strand[annot.Strand == -1] = "-"

    return pr.PyRanges(annot.dropna(axis=0))


def get_chromosome_sizes(species="mm"):
    # Get chromosome sizes for human hg38 or mouse mm10
    target_url = ""
    if species == "mm":
        # Set URL to mouse
        target_url = (
            "https://hgdownload.soe.ucsc.edu/"
            "goldenPath/mm10/bigZips/mm10.chrom.sizes"
        )
    elif species == "hg":
        # Set to human
        target_url = (
            "http://hgdownload.cse.ucsc.edu/" "goldenPath/hg38/bigZips/hg38.chrom.sizes"
        )
    else:
        raise ValueError("Unknown species")

    chrom_sizes = pd.read_table(target_url, names=["Chromosome", "End"])
    chrom_sizes["Start"] = 0
    # Reorder columns
    chrom_sizes = chrom_sizes.loc[:, ["Chromosome", "Start", "End"]]

    # Two minor conversions to agree with CellRangerARC annotations...
    # This replacement is not necessary for mouse mm10; it is for human hg38!
    if species == "hg":
        chrom_sizes["Chromosome"].replace("v", ".", inplace=True)

    # Changed long line of code to function for readability
    def __conditional_split(x):
        tokens = x.split("_")
        return tokens[1] if len(tokens) > 1 else x

    chrom_sizes["Chromosome"] = chrom_sizes["Chromosome"].apply(__conditional_split)

    # Adding a '.1' is needed for mouse, but not human
    if species == "mm":
        # Do this after the conditional split above, which chops off "chr?_"
        def __conditional_add_dot_one(x):
            return x if not x.startswith(("GL", "JH")) else x + ".1"

        chrom_sizes["Chromosome"] = chrom_sizes["Chromosome"].apply(
            __conditional_add_dot_one
        )

    # Convert DataFrame into PyRanges object for easy computation on genomic
    # coordinates
    return pr.PyRanges(chrom_sizes)


def adjust_barcodes_for_pycisTopic(data):
    """
    Adjust ATAC barcodes, as pycisTopic expects slightly different ones from
    those that come out of our scRNA-seq analysis.

    NOTE: Should be obsolete soon.
    """
    data.obs["barcode"] = data.obs.index
    # Keep only the CellRanger id
    data.obs["barcode"] = data.obs["barcode"].str.split("_", expand=True)[0]
    # Remove the increasing number
    data.obs["barcode"] = data.obs["barcode"].str.split("-", expand=True)[0]
    # Add '-1' to the barcode
    data.obs["barcode"] = data.obs["barcode"].str.cat(
        others=["1"] * len(data.obs), sep="-"
    )


def adjust_barcodes_for_scenicplus(data):
    """
    Adjust ATAC barcodes, as scenicplus expects slightly different ones from
    those that come out of our scRNA-seq analysis.

    NOTE: Should be obsolete soon.
    """
    data.obs["barcode"] = data.obs.index
    # Keep only the CellRanger id
    data.obs["barcode"] = data.obs["barcode"].str.split("_", expand=True)[0]
    # Add back the dataset name
    data.obs["barcode"] = data.obs["barcode"].str.cat(
        others=data.obs["replicate"], sep="___"
    )
    # Replace index with fixed version
    data.obs.set_index("barcode", inplace=True)
    # Get rid of 'barcode' name
    data.obs.index.rename(None, inplace=True)


def timestamp():
    """Helper function to make unique file names."""
    return datetime.datetime.now().strftime("%Y-%m-%d_%Hh%M")


def print_anndata(a):
    def __print_pandas_series(s):
        return [f"{k}: {v}" for k, v in s.items()]

    # First we use existing functionality to get an overview
    print("AnnData description:\n", a)

    print("General info:")
    # Which mice?
    print(
        f" Mouse strains:",
        __print_pandas_series(a.obs["mouse_strain_label"].value_counts()),
    )
    print(f" Experiments:", __print_pandas_series(a.obs["experiment"].value_counts()))
    # Which cortical areas?
    print(
        f" Cortical areas:",
        __print_pandas_series(a.obs["cortical_area"].value_counts()),
    )
    # Which replicates?
    print(f" Replicates: {a.obs['replicate'].unique()}")

    # Did we map cortical areas to mouse strain?
    print(
        " 'cortical_area_mouse_strain' present?",
        "cortical_area_mouse_strain" in a.obs.columns,
    )

    print("Cluster annotation:")
    # Did we already compute coarse_supertypes?
    try:
        print(" Coarse supertypes:\n", a.obs["coarse_supertype"].value_counts().values)
    except KeyError:
        print(" No coarse_supertypes present.")

    # Did we map coarse_supertypes to cortical areas?
    print(
        " 'cortical_area_coarse_supertype' present?",
        "cortical_area_coarse_supertype" in a.obs.columns,
    )

    # And supertypes?
    try:
        print(" Supertypes:\n", a.obs["supertype"].value_counts().values)
    except KeyError:
        print(" No supertypes present.")

    # Did we map supertypes to cortical areas?
    print(
        " 'cortical_area_supertype' present?",
        "cortical_area_supertype" in a.obs.columns,
    )

    if "old_supertypes" in a.obs.columns:
        print(" WARNING: 'old_supertypes' still in 'obs'")


def print_atac_configuration():
    print("INFO Settings for scATAC-seq analysis:")
    print("")

    for name in [
        "DATA_PATH",
        "REPLICATES_DATA_PATH",
        "FRAGMENTS_FILENAME",
        "BLOCKLIST_FILENAME",
        "LOCAL_BED_PATH",
        "LOCAL_BIGWIG_PATH",
        "MACS_PATH",
        "PEAK_PATH",
        "QUALITY_CONTROL_PATH",
        "FIGURES_PATH",
        "MALLET_TMP",
        "MALLET_PATH",
        "CISTOPIC_PATH",
        "RAY_TMP",
    ]:
        print(name, "=", repr(eval(name)))
    print("")

    print("metadata = ")
    pprint.pprint(metadata, compact=True)

    print("")
    print("INFO Please overwrite constants to tailor them to your needs")


def print_cistopic(c, details=False):

    if c.project is not None:
        print(f"Project: {c.project}")

    if c.fragment_matrix is not None:
        print(
            f"Fragment matrix:\n  shape (n_regions x n_cells): {c.fragment_matrix.shape}\n  type: {type(c.fragment_matrix)}"
        )

    if c.binary_matrix is not None:
        print(
            f"Binary matrix:\n  shape (n_regions x n_cells): {c.binary_matrix.shape}\n  type: {type(c.binary_matrix)}"
        )

    if c.selected_model is not None:
        print(f"Selected model:", c.selected_model)

    if c.cell_data is not None:
        print("Cell data column names:")
        pprint.pprint(c.cell_data.columns.tolist(), indent=2)

    if c.region_data is not None:
        print("Region data column names:")
        pprint.pprint(c.region_data.columns.tolist(), indent=2)

    if c.projections is not None:
        print("Projections:")
        pprint.pprint(c.projections, indent=2)

    if details:
        n_show = 2
        if c.cell_names is not None:
            print(
                f"Cell names: (n_cells {len(c.cell_names)})\n  ",
                ", ".join(c.cell_names[:n_show]),
                "...",
                ", ".join(c.cell_names[-n_show:]),
            )

        if c.region_names is not None:
            print(
                f"Region names: (n_regions {len(c.region_names)})\n  ",
                ", ".join(c.region_names[:n_show]),
                "...",
                ", ".join(c.region_names[-n_show:]),
            )
